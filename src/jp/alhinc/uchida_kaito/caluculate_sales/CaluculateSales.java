package jp.alhinc.uchida_kaito.caluculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public  class CaluculateSales {

	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
		}
		if (!inputFile(args[0], "branch.lst", "支店", "^[0-9]{3}", branchNames, branchSales)) {
			return;
		}
		if(!inputFile(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]{8}", commodityNames, commoditySales)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}[.][r][c][d]$")){
				rcdFiles.add(files[i]);
			}
		}
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			//rcdFiles内のファイルを昇順にソート
			Collections.sort(rcdFiles);
			//ファイル名の前から8桁を取得後int型にキャスト
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			//連番になっているか判定
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//ここから支店売上集計ファイル読み込み
		BufferedReader brProfit = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				brProfit = new BufferedReader(fr);
				String line;
				List<String> salesRecord = new ArrayList<>();

				while((line = brProfit.readLine()) != null) {
					salesRecord.add(line);
				}
				//売上集計ファイルの中身が3行かどうか判定
				if(salesRecord.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() +"のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイル内にあるか判定
				if (!branchSales.containsKey(salesRecord.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				//売上ファイルの商品コードが商品定義ファイル内にあるか判定
				if (!commoditySales.containsKey(salesRecord.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//売上ファイルの売上項目が1-9桁の数字になっているか判定
				if (!salesRecord.get(2).matches( "^[0-9]{1,9}")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//ファイル内の売上をlong型でsaleに代入
				long sale = Long.parseLong(salesRecord.get(2));
				//branchSales内にある同支店の売上を抽出し、足し合わせる
				long branchResult = branchSales.get(salesRecord.get(0))+ sale;
				//branchSales内にある同商品の売上を抽出し、足し合わせる
				long commodityResult = commoditySales.get(salesRecord.get(1)) + sale;
				//集計した支店別売上金額が10桁を超えるか判定
				if(branchResult >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//集計した商品別売上金額が10桁を超えるとか判定
				if(commodityResult >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(salesRecord.get(0), branchResult);
				commoditySales.put(salesRecord.get(1), commodityResult);
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}finally {
			    if(brProfit != null) {
			    	try {
			    		brProfit.close();
			    	}catch(IOException e) {
			    		System.out.println("予期せぬエラーが発生しました");
			    		return;
			    	}
				}
			}
		}
		if(!outputSales(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if(!outputSales(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}
	public static boolean outputSales(String path, String fileName,Map<String, String> namesFile, Map<String, Long>salesFile) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : namesFile.keySet()) {
				bw.write(key + "," + namesFile.get(key) + "," + salesFile.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		}finally{
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
	public static boolean inputFile(String path, String fileName, String type, String regex, Map<String, String> fileNames, Map<String, Long> fileSales ) {
		BufferedReader brBranch = null;
		//定義ファイルの読み込み
		try {
 			File file = new File(path,fileName);
			//定義ファイルが存在するか判定
			if (!file.exists()) {
				System.out.println(type + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			brBranch = new BufferedReader(fr);
			String line;
			while((line = brBranch.readLine()) != null) {
				String[] items = line.split(",");
				//ファイル内のフォーマットが正しいか判定
				if((items.length != 2) || (!items[0].matches(regex)) ) {
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}
				fileNames.put(items[0], items[1]);
				fileSales.put(items[0],0L);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		}finally {
			if(brBranch != null) {
				try {
					brBranch.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}


